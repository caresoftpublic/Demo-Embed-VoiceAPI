<?php
// $hoster = "https://capi.caresoft.vn/";
// $hoster = "https://test.caresoft.vn/";
$hoster = "http://localhost:8000";
set_time_limit(60);
?>

<html>
<head>
    <!--    <script src="--><?php //= $hoster ?><!--/js/embed/jquery.min.js" type="text/javascript"></script>-->
    <script src="<?= $hoster ?>/js/embed/jssip-3.2.10.js" type="text/javascript"></script>
    <script src="<?= $hoster ?>/js/embed/init-3.0.7.js" type="text/javascript"></script>
    <script src="<?= $hoster ?>/js/embed/web.push.js" type="text/javascript"></script>
    <script src="<?= $hoster ?>/js/embed/cs_const.js" type="text/javascript"></script>
    <script src="<?= $hoster ?>/js/embed/cs_voice.js" type="text/javascript"></script>

    <script src="handle_voice_script.js" type="text/javascript"></script>

    <script>
        csInit("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpcHBob25lIjoiNTAwMCJ9.iWGOA-MpUjlb_q6LNFFuOkjJUwqpL5UWTBM4l6tOrlY", "vi2");
    </script>
    <script>
        function onTransferCallSurvey() {
            transferSurvey({id: 'survey_id', sipurl: 'surveySipURL'});
        }

        function transferCall() {
            getTransferAgent();
            csTransferCallAgent('5000');
        }

        function transferCallToAcd() {
            csTransferCallAcd('QUEUE_ID');
        }


        function onCallout() {
            const calloutId = document.getElementById('select-call-out-id').value;
            if (calloutId) {
                console.log('calling with callout service: ', calloutId);
                csCallout(document.getElementById('phoneNumber').value, calloutId);

            } else {
                console.log('calling with default callout service');
                csCallout(document.getElementById('phoneNumber').value);
            }
        }
    </script>
</head>
<body>
<video id="my-video" autoplay style="display: none;" src="<?= $hoster ?>/images/320x240.ogg">
</video>
<video id="peer-video" autoplay style="display: none;" src="<?= $hoster ?>/images/320x240.ogg">
</video>

<button id="enable" onclick="csEnableCall()">1. Kích hoạt thoại</button>
<br/>
<button id="enable" onclick="changeCallStatus()">2. On/Off trạng thái</button>
, trạng thái hiện tại: <span id="onOffIncicator"></span>
<br/>
<div style="margin: 10px 0">
    <label for="select-call-out-id">Dịch vụ</label>
    <select id="select-call-out-id" value="">
    </select>
</div>
<input type="text" id="phoneNumber"/>
<button onclick="onCallout()">3. Gọi ra</button>
<br/>
<button onclick="endCall();">4.Kết thúc cuộc gọi ra</button>
<br/><br/><br/><br/>

<label id="phoneNo"></label><br/>
<button onclick="onAcceptCall();">1. Nhận cuộc gọi vào</button>
<br/>
<button onclick="muteCall();">2.Mute/Unmute</button>
<br/>
<button onclick="holdCall();">3.Hold/Unhold</button>
<br/>
<button onclick="endCall();">4.Kết thúc cuộc gọi vào</button>
<br/>
<button onclick="transferCall();" id="transferCall" disabled>5.Chuyển cuộc gọi</button>
<br/>
<button onclick="transferCallToAcd();" id="transferCallAcd" disabled>6.Chuyển cuộc gọi sang nhánh acd</button>
<br/>
<button onclick="responseTransferAgent(1);" id="transferResponseOK" disabled>7.Chấp nhận chuyển cg</button>
<br/>
<button onclick="responseTransferAgent(0);" id="transferResponseReject" disabled>8.Từ chối chuyển cg</button>
<br/>
<button onclick="onTransferCallSurvey()" id="transferSurvey" disabled>9.Chuyển survey thoại (Ngắt cg hiện tại)</button>
<br/>
</body>
</html>