<?php
$hoster = "https://capi.caresoft.vn";

?>

<html>
    <head>
        <script src="<?= $hoster ?>/js/embed/jquery.min.js" type="text/javascript"></script>    
        <script src="<?= $hoster ?>/js/embed/jssip-3.2.10.js" type="text/javascript"></script>
        <script src="<?= $hoster ?>/js/embed/init-3.0.7.js" type="text/javascript"></script>
        <script src="<?= $hoster ?>/js/embed/web.push.js" type="text/javascript"></script>
        <script src="<?= $hoster ?>/js/embed/cs_const.js" type="text/javascript"></script>
        <script src="<?= $hoster ?>/js/embed/cs_voice.js" type="text/javascript"></script>

        <script src="handle_voice_script.js" type="text/javascript"></script>
        <script>
           csInit("your_registered_token", "your_domain");
        
        </script>
        <script>
            function onTransferCallSurvey() {
                transferSurvey({id: 'survey_id', sipurl: 'surveySipURL'});
            }

            function transferCall() {
                getTransferAgent();
                csTransferCallAgent('5000');
            }

            function transferCallToAcd() {
                csTransferCallAcd('QUEUE_ID');
            }


            function onCallout() {
                const calloutId = document.getElementById('select-call-out-id').value;
                if (calloutId) {
                    console.log('calling with callout service: ', calloutId);
                    csCallout(document.getElementById('phoneNumber').value, calloutId);

                } else {
                    console.log('calling with default callout service');
                    csCallout(document.getElementById('phoneNumber').value);
                }
            }
        </script>
    </head>
    <body>
        <video id="my-video" autoplay style="display: none;" src="<?=$hoster?>/images/320x240.ogg">
        </video>
        <video id="peer-video" autoplay style="display: none;" src="<?=$hoster?>/images/320x240.ogg">
        </video>
        
        <button id="enable" onclick="csEnableCall()">1. Activate call </button><br/>
        <button id="enable" onclick="changeCallStatus()">2. On/Off agent status</button><br/>
        <div style="margin: 10px 0">
            <label for="select-call-out-id">Callout service</label>
            <select id="select-call-out-id" value="">
            </select>
        </div>
        <input type="text" id="phoneNumber"/><button onclick="onCallout()">3. Call out</button><br/>
        <button onclick="endCall();">4.End call out</button><br/><br/><br/><br/>

        <label id="phoneNo"></label><br/>
        <button onclick="onAcceptCall();">. Receive call</button><br/>
        <button onclick="muteCall();">.Mute/Unmute</button><br/>
        <button onclick="holdCall();">.Hold/Unhold</button><br/>
        <button onclick="endCall();">.End call</button><br/>
        <button onclick="transferCall();" id="transferCall" disabled>.Transfer call</button><br/>
        <button onclick="transferCallToAcd();" id="transferCallAcd" disabled>.transfer call to an acd</button><br/>
        <button onclick="responseTransferAgent(1);" id="transferResponseOK" disabled>.Accept transfered call</button><br/>
        <button onclick="responseTransferAgent(0);" id="transferResponseReject" disabled>.Refuse transfered call</button><br/>
		<button onclick="onTransferCallSurvey()" id="transferSurvey" disabled>.Transfer survey (will terminate current call)</button>
<br/>
    </body>
</html>