window.onbeforeunload = function (e) {
    return 'abc';
};

window.unload = function () {
    csUnregister();
    if (csVoice.enableVoice) {
        reConfigDeviceType();
    }
    return "1";
}

// kết thúc cuộc gọi ra/vào
function csEndCall() {
    console.log("Call is ended");
    document.getElementById('phoneNo').innerHTML = "";

    document.getElementById('transferCall').setAttribute('disabled', 'disabled');
    document.getElementById('transferCallAcd').setAttribute('disabled', 'disabled');
    document.getElementById('transferSurvey').setAttribute('disabled', 'disabled');
}

// đổ chuông trình duyệt của agent khi gọi vào
// đổ chuông tới khách hàng khi gọi ra
function csCallRinging(phone) {
    console.log("Has a new call to customer: " + phone);
    document.getElementById('phoneNo').innerHTML = phone + ' đang gọi ...';
}

// cuộc gọi vào được agent trả lời
function csAcceptCall() {
    console.log("Call is Accepted");
    document.getElementById('phoneNo').innerHTML = "Đang trả lời";
    document.getElementById('transferCall').removeAttribute('disabled');
    document.getElementById('transferCallAcd').removeAttribute('disabled');
    document.getElementById('transferSurvey').removeAttribute('disabled');

}

// cuộc gọi ra được khách hàng trả lời
function csCustomerAccept() {
    console.log("csCustomerAccept");
    document.getElementById('phoneNo').innerHTML = "Đang trả lời";
}

function csMuteCall() {
    console.log("Call is muted");
}

function csUnMuteCall() {
    console.log("Call is unmuted")
}

function csHoldCall() {
    console.log("Call is holded");
}

function csUnHoldCall() {
    console.log("Call is unholded");
}

function showCalloutInfo(number) {
    console.log("callout to " + number);
}

function showCalloutError(errorCode, sipCode) {
    console.log("callout error " + errorCode + " - " + sipCode);
}

function csShowEnableVoice(enableVoice) {
    console.log(`Voice active status : ${enableVoice}`);
    if (enableVoice) {
        document.getElementById('enable').setAttribute('disabled', 'disabled');
    } else {
        document.getElementById('enable').removeAttribute('disabled');
    }
}

function csShowDeviceType(type) {
    console.log(`Current device: ${type}`);
}

function csShowCallStatus(status) {
    console.log("csShowCallStatus");
    document.getElementById('onOffIncicator').innerHTML = status;
}

function csInitComplete() {
    console.log("csInitComplete");
    const lstCallout = csVoice.getCalloutServices();
    const defaultCalloutId = (lstCallout.find(c => c.is_default == 1) || {}).callout_id;
    const selectEl = document.querySelector("#select-call-out-id");
    lstCallout.forEach(c => {
        const calloutOptions = document.createElement("option");
        calloutOptions.value = c.callout_id;
        calloutOptions.text = c.descriptions;
        selectEl.appendChild(calloutOptions);
    });

    if (defaultCalloutId) {
        selectEl.value = defaultCalloutId;
    }
}

function csCurrentCallId(callId) {
    console.log("csCurrentCallId: " + callId);
}

function csInitError(error) {
    console.log("csInitError: " + error);
}

function csListTransferAgent(listTransferAgent) {
    console.log(listTransferAgent);
}

function csTransferCallError(error, tranferedAgentInfo) {
    console.log('Transfer call failed,' + error);
}

function csTransferCallSuccess(tranferedAgentInfo) {
    console.log('transfer call success');
}

function csNewCallTransferRequest(transferCall) {
    console.log('new call transfer');
    console.log(transferCall);
    document.getElementById('phoneNo').innerHTML = transferCall.dropAgentId + ' chuyển cg cho bạn';
    document.getElementById('transferResponseOK').removeAttribute('disabled');
    document.getElementById('transferResponseReject').removeAttribute('disabled');
}

function csTransferCallResponse(status) {
    document.getElementById('transferResponseOK').setAttribute('disabled', 'disabled');
    document.getElementById('transferResponseReject').setAttribute('disabled', 'disabled');
    console.log(status);
}

function csNotifyReconnecting(noretry, totalRetry) {
    console.log('reconnecting from custom js......');
}

function csOndisconnected() {
    console.log('disconnected from custom js !!!!!!!!');
}

